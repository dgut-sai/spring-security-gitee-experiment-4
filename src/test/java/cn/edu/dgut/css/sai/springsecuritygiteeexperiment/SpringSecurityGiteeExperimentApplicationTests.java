package cn.edu.dgut.css.sai.springsecuritygiteeexperiment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
class SpringSecurityGiteeExperimentApplicationTests {

    ///// Setting Up MockMvc and Spring Security. start
    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    // 在本类中，每次执行测试都先调用@BeforeEach注解的方法
    @BeforeEach
    public void init() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }
    ///// Setting Up MockMvc and Spring Security. end


    /**
     * 测试限权接口
     * <p></p>
     * 模拟一个登录用户，访问受保护的接口。
     * <p></p>
     * @see WithMockUser
     * @see MockMvc#perform(RequestBuilder)
     * @see MockMvcRequestBuilders#get(String, Object...)
     */
    @Test
    public void test() throws Exception {
        ////////////////////////////////////////////
        /// 步骤八：模拟一个登录用户，访问受保护的接口/test，断言接口的返回内容body部分是否一致。
        ////////////////////////////////////////////
    }

}
